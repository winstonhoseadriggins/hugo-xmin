---
title: Home
---

[<img src="https://simpleicons.org/icons/github.svg" style="max-width:15%;min-width:40px;float:right;" alt="Github repo" />](https://gitlab.com/winstonhoseadriggins/hugo-xmin)

# DW84 Inc LLC Foundation

## Autobiographical portfolio

# Development of Blockchain Database Applications

The beauty of the blockchain is its distributed architecture.
Once A Transaction is verified by the blockchain, A Permanent Digital Signature Will Propogate Throughout Eternity.

DW84 Inc LLC Foundation Special Agent primary role is the security and storage of client digital assets.
## Increase Visibility  

## Establish Brand Recognition

DW84 Inc LLC Foundation Reconciliation Agent primary role is to assist the client in recovery plus discovery of digital assets.

DW84 Inc LLC Foundation Reconciliation Agent secondary role is to assist the client in crafting a digital identity.
## CRYPTOCURRENCY 

Propogate and Preserve Your Digital Assets With Blockchain Technologies.

DW84 Inc LLC Foundation Acquisition Agent primary
role is the integration of "smart contracts" with 
client digital assets.